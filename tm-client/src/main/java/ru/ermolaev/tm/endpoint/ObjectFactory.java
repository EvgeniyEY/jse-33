
package ru.ermolaev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ermolaev.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "Exception");
    private final static QName _ClearBase64File_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearBase64File");
    private final static QName _ClearBase64FileResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearBase64FileResponse");
    private final static QName _ClearBinaryFile_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearBinaryFile");
    private final static QName _ClearBinaryFileResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearBinaryFileResponse");
    private final static QName _ClearJsonFileFasterXml_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearJsonFileFasterXml");
    private final static QName _ClearJsonFileFasterXmlResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearJsonFileFasterXmlResponse");
    private final static QName _ClearJsonFileJaxb_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearJsonFileJaxb");
    private final static QName _ClearJsonFileJaxbResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearJsonFileJaxbResponse");
    private final static QName _ClearXmlFileFasterXml_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearXmlFileFasterXml");
    private final static QName _ClearXmlFileFasterXmlResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearXmlFileFasterXmlResponse");
    private final static QName _ClearXmlFileJaxb_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearXmlFileJaxb");
    private final static QName _ClearXmlFileJaxbResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "clearXmlFileJaxbResponse");
    private final static QName _LoadBase64_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadBase64");
    private final static QName _LoadBase64Response_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadBase64Response");
    private final static QName _LoadBinary_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadBinary");
    private final static QName _LoadBinaryResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadBinaryResponse");
    private final static QName _LoadJsonByFasterXml_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadJsonByFasterXml");
    private final static QName _LoadJsonByFasterXmlResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadJsonByFasterXmlResponse");
    private final static QName _LoadJsonByJaxb_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadJsonByJaxb");
    private final static QName _LoadJsonByJaxbResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadJsonByJaxbResponse");
    private final static QName _LoadXmlByFasterXml_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadXmlByFasterXml");
    private final static QName _LoadXmlByFasterXmlResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadXmlByFasterXmlResponse");
    private final static QName _LoadXmlByJaxb_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadXmlByJaxb");
    private final static QName _LoadXmlByJaxbResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "loadXmlByJaxbResponse");
    private final static QName _SaveBase64_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveBase64");
    private final static QName _SaveBase64Response_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveBase64Response");
    private final static QName _SaveBinary_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveBinary");
    private final static QName _SaveBinaryResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveBinaryResponse");
    private final static QName _SaveJsonByFasterXml_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveJsonByFasterXml");
    private final static QName _SaveJsonByFasterXmlResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveJsonByFasterXmlResponse");
    private final static QName _SaveJsonByJaxb_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveJsonByJaxb");
    private final static QName _SaveJsonByJaxbResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveJsonByJaxbResponse");
    private final static QName _SaveXmlByFasterXml_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveXmlByFasterXml");
    private final static QName _SaveXmlByFasterXmlResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveXmlByFasterXmlResponse");
    private final static QName _SaveXmlByJaxb_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveXmlByJaxb");
    private final static QName _SaveXmlByJaxbResponse_QNAME = new QName("http://endpoint.tm.ermolaev.ru/", "saveXmlByJaxbResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ermolaev.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ClearBase64File }
     * 
     */
    public ClearBase64File createClearBase64File() {
        return new ClearBase64File();
    }

    /**
     * Create an instance of {@link ClearBase64FileResponse }
     * 
     */
    public ClearBase64FileResponse createClearBase64FileResponse() {
        return new ClearBase64FileResponse();
    }

    /**
     * Create an instance of {@link ClearBinaryFile }
     * 
     */
    public ClearBinaryFile createClearBinaryFile() {
        return new ClearBinaryFile();
    }

    /**
     * Create an instance of {@link ClearBinaryFileResponse }
     * 
     */
    public ClearBinaryFileResponse createClearBinaryFileResponse() {
        return new ClearBinaryFileResponse();
    }

    /**
     * Create an instance of {@link ClearJsonFileFasterXml }
     * 
     */
    public ClearJsonFileFasterXml createClearJsonFileFasterXml() {
        return new ClearJsonFileFasterXml();
    }

    /**
     * Create an instance of {@link ClearJsonFileFasterXmlResponse }
     * 
     */
    public ClearJsonFileFasterXmlResponse createClearJsonFileFasterXmlResponse() {
        return new ClearJsonFileFasterXmlResponse();
    }

    /**
     * Create an instance of {@link ClearJsonFileJaxb }
     * 
     */
    public ClearJsonFileJaxb createClearJsonFileJaxb() {
        return new ClearJsonFileJaxb();
    }

    /**
     * Create an instance of {@link ClearJsonFileJaxbResponse }
     * 
     */
    public ClearJsonFileJaxbResponse createClearJsonFileJaxbResponse() {
        return new ClearJsonFileJaxbResponse();
    }

    /**
     * Create an instance of {@link ClearXmlFileFasterXml }
     * 
     */
    public ClearXmlFileFasterXml createClearXmlFileFasterXml() {
        return new ClearXmlFileFasterXml();
    }

    /**
     * Create an instance of {@link ClearXmlFileFasterXmlResponse }
     * 
     */
    public ClearXmlFileFasterXmlResponse createClearXmlFileFasterXmlResponse() {
        return new ClearXmlFileFasterXmlResponse();
    }

    /**
     * Create an instance of {@link ClearXmlFileJaxb }
     * 
     */
    public ClearXmlFileJaxb createClearXmlFileJaxb() {
        return new ClearXmlFileJaxb();
    }

    /**
     * Create an instance of {@link ClearXmlFileJaxbResponse }
     * 
     */
    public ClearXmlFileJaxbResponse createClearXmlFileJaxbResponse() {
        return new ClearXmlFileJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadBase64 }
     * 
     */
    public LoadBase64 createLoadBase64() {
        return new LoadBase64();
    }

    /**
     * Create an instance of {@link LoadBase64Response }
     * 
     */
    public LoadBase64Response createLoadBase64Response() {
        return new LoadBase64Response();
    }

    /**
     * Create an instance of {@link LoadBinary }
     * 
     */
    public LoadBinary createLoadBinary() {
        return new LoadBinary();
    }

    /**
     * Create an instance of {@link LoadBinaryResponse }
     * 
     */
    public LoadBinaryResponse createLoadBinaryResponse() {
        return new LoadBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadJsonByFasterXml }
     * 
     */
    public LoadJsonByFasterXml createLoadJsonByFasterXml() {
        return new LoadJsonByFasterXml();
    }

    /**
     * Create an instance of {@link LoadJsonByFasterXmlResponse }
     * 
     */
    public LoadJsonByFasterXmlResponse createLoadJsonByFasterXmlResponse() {
        return new LoadJsonByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadJsonByJaxb }
     * 
     */
    public LoadJsonByJaxb createLoadJsonByJaxb() {
        return new LoadJsonByJaxb();
    }

    /**
     * Create an instance of {@link LoadJsonByJaxbResponse }
     * 
     */
    public LoadJsonByJaxbResponse createLoadJsonByJaxbResponse() {
        return new LoadJsonByJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadXmlByFasterXml }
     * 
     */
    public LoadXmlByFasterXml createLoadXmlByFasterXml() {
        return new LoadXmlByFasterXml();
    }

    /**
     * Create an instance of {@link LoadXmlByFasterXmlResponse }
     * 
     */
    public LoadXmlByFasterXmlResponse createLoadXmlByFasterXmlResponse() {
        return new LoadXmlByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadXmlByJaxb }
     * 
     */
    public LoadXmlByJaxb createLoadXmlByJaxb() {
        return new LoadXmlByJaxb();
    }

    /**
     * Create an instance of {@link LoadXmlByJaxbResponse }
     * 
     */
    public LoadXmlByJaxbResponse createLoadXmlByJaxbResponse() {
        return new LoadXmlByJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveBase64 }
     * 
     */
    public SaveBase64 createSaveBase64() {
        return new SaveBase64();
    }

    /**
     * Create an instance of {@link SaveBase64Response }
     * 
     */
    public SaveBase64Response createSaveBase64Response() {
        return new SaveBase64Response();
    }

    /**
     * Create an instance of {@link SaveBinary }
     * 
     */
    public SaveBinary createSaveBinary() {
        return new SaveBinary();
    }

    /**
     * Create an instance of {@link SaveBinaryResponse }
     * 
     */
    public SaveBinaryResponse createSaveBinaryResponse() {
        return new SaveBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveJsonByFasterXml }
     * 
     */
    public SaveJsonByFasterXml createSaveJsonByFasterXml() {
        return new SaveJsonByFasterXml();
    }

    /**
     * Create an instance of {@link SaveJsonByFasterXmlResponse }
     * 
     */
    public SaveJsonByFasterXmlResponse createSaveJsonByFasterXmlResponse() {
        return new SaveJsonByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveJsonByJaxb }
     * 
     */
    public SaveJsonByJaxb createSaveJsonByJaxb() {
        return new SaveJsonByJaxb();
    }

    /**
     * Create an instance of {@link SaveJsonByJaxbResponse }
     * 
     */
    public SaveJsonByJaxbResponse createSaveJsonByJaxbResponse() {
        return new SaveJsonByJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveXmlByFasterXml }
     * 
     */
    public SaveXmlByFasterXml createSaveXmlByFasterXml() {
        return new SaveXmlByFasterXml();
    }

    /**
     * Create an instance of {@link SaveXmlByFasterXmlResponse }
     * 
     */
    public SaveXmlByFasterXmlResponse createSaveXmlByFasterXmlResponse() {
        return new SaveXmlByFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveXmlByJaxb }
     * 
     */
    public SaveXmlByJaxb createSaveXmlByJaxb() {
        return new SaveXmlByJaxb();
    }

    /**
     * Create an instance of {@link SaveXmlByJaxbResponse }
     * 
     */
    public SaveXmlByJaxbResponse createSaveXmlByJaxbResponse() {
        return new SaveXmlByJaxbResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearBase64File }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearBase64File")
    public JAXBElement<ClearBase64File> createClearBase64File(ClearBase64File value) {
        return new JAXBElement<ClearBase64File>(_ClearBase64File_QNAME, ClearBase64File.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearBase64FileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearBase64FileResponse")
    public JAXBElement<ClearBase64FileResponse> createClearBase64FileResponse(ClearBase64FileResponse value) {
        return new JAXBElement<ClearBase64FileResponse>(_ClearBase64FileResponse_QNAME, ClearBase64FileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearBinaryFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearBinaryFile")
    public JAXBElement<ClearBinaryFile> createClearBinaryFile(ClearBinaryFile value) {
        return new JAXBElement<ClearBinaryFile>(_ClearBinaryFile_QNAME, ClearBinaryFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearBinaryFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearBinaryFileResponse")
    public JAXBElement<ClearBinaryFileResponse> createClearBinaryFileResponse(ClearBinaryFileResponse value) {
        return new JAXBElement<ClearBinaryFileResponse>(_ClearBinaryFileResponse_QNAME, ClearBinaryFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearJsonFileFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearJsonFileFasterXml")
    public JAXBElement<ClearJsonFileFasterXml> createClearJsonFileFasterXml(ClearJsonFileFasterXml value) {
        return new JAXBElement<ClearJsonFileFasterXml>(_ClearJsonFileFasterXml_QNAME, ClearJsonFileFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearJsonFileFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearJsonFileFasterXmlResponse")
    public JAXBElement<ClearJsonFileFasterXmlResponse> createClearJsonFileFasterXmlResponse(ClearJsonFileFasterXmlResponse value) {
        return new JAXBElement<ClearJsonFileFasterXmlResponse>(_ClearJsonFileFasterXmlResponse_QNAME, ClearJsonFileFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearJsonFileJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearJsonFileJaxb")
    public JAXBElement<ClearJsonFileJaxb> createClearJsonFileJaxb(ClearJsonFileJaxb value) {
        return new JAXBElement<ClearJsonFileJaxb>(_ClearJsonFileJaxb_QNAME, ClearJsonFileJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearJsonFileJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearJsonFileJaxbResponse")
    public JAXBElement<ClearJsonFileJaxbResponse> createClearJsonFileJaxbResponse(ClearJsonFileJaxbResponse value) {
        return new JAXBElement<ClearJsonFileJaxbResponse>(_ClearJsonFileJaxbResponse_QNAME, ClearJsonFileJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearXmlFileFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearXmlFileFasterXml")
    public JAXBElement<ClearXmlFileFasterXml> createClearXmlFileFasterXml(ClearXmlFileFasterXml value) {
        return new JAXBElement<ClearXmlFileFasterXml>(_ClearXmlFileFasterXml_QNAME, ClearXmlFileFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearXmlFileFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearXmlFileFasterXmlResponse")
    public JAXBElement<ClearXmlFileFasterXmlResponse> createClearXmlFileFasterXmlResponse(ClearXmlFileFasterXmlResponse value) {
        return new JAXBElement<ClearXmlFileFasterXmlResponse>(_ClearXmlFileFasterXmlResponse_QNAME, ClearXmlFileFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearXmlFileJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearXmlFileJaxb")
    public JAXBElement<ClearXmlFileJaxb> createClearXmlFileJaxb(ClearXmlFileJaxb value) {
        return new JAXBElement<ClearXmlFileJaxb>(_ClearXmlFileJaxb_QNAME, ClearXmlFileJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearXmlFileJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "clearXmlFileJaxbResponse")
    public JAXBElement<ClearXmlFileJaxbResponse> createClearXmlFileJaxbResponse(ClearXmlFileJaxbResponse value) {
        return new JAXBElement<ClearXmlFileJaxbResponse>(_ClearXmlFileJaxbResponse_QNAME, ClearXmlFileJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadBase64")
    public JAXBElement<LoadBase64> createLoadBase64(LoadBase64 value) {
        return new JAXBElement<LoadBase64>(_LoadBase64_QNAME, LoadBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadBase64Response")
    public JAXBElement<LoadBase64Response> createLoadBase64Response(LoadBase64Response value) {
        return new JAXBElement<LoadBase64Response>(_LoadBase64Response_QNAME, LoadBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadBinary")
    public JAXBElement<LoadBinary> createLoadBinary(LoadBinary value) {
        return new JAXBElement<LoadBinary>(_LoadBinary_QNAME, LoadBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadBinaryResponse")
    public JAXBElement<LoadBinaryResponse> createLoadBinaryResponse(LoadBinaryResponse value) {
        return new JAXBElement<LoadBinaryResponse>(_LoadBinaryResponse_QNAME, LoadBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadJsonByFasterXml")
    public JAXBElement<LoadJsonByFasterXml> createLoadJsonByFasterXml(LoadJsonByFasterXml value) {
        return new JAXBElement<LoadJsonByFasterXml>(_LoadJsonByFasterXml_QNAME, LoadJsonByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadJsonByFasterXmlResponse")
    public JAXBElement<LoadJsonByFasterXmlResponse> createLoadJsonByFasterXmlResponse(LoadJsonByFasterXmlResponse value) {
        return new JAXBElement<LoadJsonByFasterXmlResponse>(_LoadJsonByFasterXmlResponse_QNAME, LoadJsonByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonByJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadJsonByJaxb")
    public JAXBElement<LoadJsonByJaxb> createLoadJsonByJaxb(LoadJsonByJaxb value) {
        return new JAXBElement<LoadJsonByJaxb>(_LoadJsonByJaxb_QNAME, LoadJsonByJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJsonByJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadJsonByJaxbResponse")
    public JAXBElement<LoadJsonByJaxbResponse> createLoadJsonByJaxbResponse(LoadJsonByJaxbResponse value) {
        return new JAXBElement<LoadJsonByJaxbResponse>(_LoadJsonByJaxbResponse_QNAME, LoadJsonByJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadXmlByFasterXml")
    public JAXBElement<LoadXmlByFasterXml> createLoadXmlByFasterXml(LoadXmlByFasterXml value) {
        return new JAXBElement<LoadXmlByFasterXml>(_LoadXmlByFasterXml_QNAME, LoadXmlByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadXmlByFasterXmlResponse")
    public JAXBElement<LoadXmlByFasterXmlResponse> createLoadXmlByFasterXmlResponse(LoadXmlByFasterXmlResponse value) {
        return new JAXBElement<LoadXmlByFasterXmlResponse>(_LoadXmlByFasterXmlResponse_QNAME, LoadXmlByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlByJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadXmlByJaxb")
    public JAXBElement<LoadXmlByJaxb> createLoadXmlByJaxb(LoadXmlByJaxb value) {
        return new JAXBElement<LoadXmlByJaxb>(_LoadXmlByJaxb_QNAME, LoadXmlByJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadXmlByJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "loadXmlByJaxbResponse")
    public JAXBElement<LoadXmlByJaxbResponse> createLoadXmlByJaxbResponse(LoadXmlByJaxbResponse value) {
        return new JAXBElement<LoadXmlByJaxbResponse>(_LoadXmlByJaxbResponse_QNAME, LoadXmlByJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveBase64")
    public JAXBElement<SaveBase64> createSaveBase64(SaveBase64 value) {
        return new JAXBElement<SaveBase64>(_SaveBase64_QNAME, SaveBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveBase64Response")
    public JAXBElement<SaveBase64Response> createSaveBase64Response(SaveBase64Response value) {
        return new JAXBElement<SaveBase64Response>(_SaveBase64Response_QNAME, SaveBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveBinary")
    public JAXBElement<SaveBinary> createSaveBinary(SaveBinary value) {
        return new JAXBElement<SaveBinary>(_SaveBinary_QNAME, SaveBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveBinaryResponse")
    public JAXBElement<SaveBinaryResponse> createSaveBinaryResponse(SaveBinaryResponse value) {
        return new JAXBElement<SaveBinaryResponse>(_SaveBinaryResponse_QNAME, SaveBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveJsonByFasterXml")
    public JAXBElement<SaveJsonByFasterXml> createSaveJsonByFasterXml(SaveJsonByFasterXml value) {
        return new JAXBElement<SaveJsonByFasterXml>(_SaveJsonByFasterXml_QNAME, SaveJsonByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveJsonByFasterXmlResponse")
    public JAXBElement<SaveJsonByFasterXmlResponse> createSaveJsonByFasterXmlResponse(SaveJsonByFasterXmlResponse value) {
        return new JAXBElement<SaveJsonByFasterXmlResponse>(_SaveJsonByFasterXmlResponse_QNAME, SaveJsonByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonByJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveJsonByJaxb")
    public JAXBElement<SaveJsonByJaxb> createSaveJsonByJaxb(SaveJsonByJaxb value) {
        return new JAXBElement<SaveJsonByJaxb>(_SaveJsonByJaxb_QNAME, SaveJsonByJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJsonByJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveJsonByJaxbResponse")
    public JAXBElement<SaveJsonByJaxbResponse> createSaveJsonByJaxbResponse(SaveJsonByJaxbResponse value) {
        return new JAXBElement<SaveJsonByJaxbResponse>(_SaveJsonByJaxbResponse_QNAME, SaveJsonByJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlByFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveXmlByFasterXml")
    public JAXBElement<SaveXmlByFasterXml> createSaveXmlByFasterXml(SaveXmlByFasterXml value) {
        return new JAXBElement<SaveXmlByFasterXml>(_SaveXmlByFasterXml_QNAME, SaveXmlByFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlByFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveXmlByFasterXmlResponse")
    public JAXBElement<SaveXmlByFasterXmlResponse> createSaveXmlByFasterXmlResponse(SaveXmlByFasterXmlResponse value) {
        return new JAXBElement<SaveXmlByFasterXmlResponse>(_SaveXmlByFasterXmlResponse_QNAME, SaveXmlByFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlByJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveXmlByJaxb")
    public JAXBElement<SaveXmlByJaxb> createSaveXmlByJaxb(SaveXmlByJaxb value) {
        return new JAXBElement<SaveXmlByJaxb>(_SaveXmlByJaxb_QNAME, SaveXmlByJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveXmlByJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ermolaev.ru/", name = "saveXmlByJaxbResponse")
    public JAXBElement<SaveXmlByJaxbResponse> createSaveXmlByJaxbResponse(SaveXmlByJaxbResponse value) {
        return new JAXBElement<SaveXmlByJaxbResponse>(_SaveXmlByJaxbResponse_QNAME, SaveXmlByJaxbResponse.class, null, value);
    }

}
