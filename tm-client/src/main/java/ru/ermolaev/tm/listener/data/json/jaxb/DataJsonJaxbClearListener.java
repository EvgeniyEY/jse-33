package ru.ermolaev.tm.listener.data.json.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.data.AbstractDataListener;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataJsonJaxbClearListener extends AbstractDataListener {

    @Autowired
    public DataJsonJaxbClearListener(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return "data-json-jb-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json (Jax-B) file.";
    }

    @Override
    @EventListener(condition = "@dataJsonJaxbClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE JSON (JAX-B) FILE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.clearJsonFileJaxb(session);
        System.out.println("[OK]");
    }

}
