package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.AdminUserEndpoint;

@Component
public abstract class AbstractAdminListener extends AbstractListener {

    protected AdminUserEndpoint adminUserEndpoint;

    @Autowired
    public AbstractAdminListener(
            @NotNull final AdminUserEndpoint adminUserEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.adminUserEndpoint = adminUserEndpoint;
    }

}
