package ru.ermolaev.tm.listener.data.xml.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.data.AbstractDataListener;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataXmlJaxbClearListener extends AbstractDataListener {

    @Autowired
    public DataXmlJaxbClearListener(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return "data-xml-jb-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove XML (Jax-B) file.";
    }

    @Override
    @EventListener(condition = "@dataXmlJaxbClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE XML (JAX-B) FILE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.clearXmlFileJaxb(session);
        System.out.println("[OK]");
    }

}
