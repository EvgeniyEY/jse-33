package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.user.AccessDeniedException;
import ru.ermolaev.tm.util.HashUtil;
import ru.ermolaev.tm.util.SignatureUtil;
import ru.ermolaev.tm.api.service.IPropertyService;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.api.service.IUserService;

import java.util.List;

@Service
public class SessionService extends AbstractService<Session> implements ISessionService {

    private final ISessionRepository sessionRepository;

    private final IUserService userService;

    private final IPropertyService propertyService;

    @Autowired
    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.sessionRepository = sessionRepository;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    @Transactional(readOnly = true)
    public void validate(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new AccessDeniedException();
        if (sessionDTO.getSignature() == null) throw new AccessDeniedException();
        if (sessionDTO.getStartTime() == null) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = sessionDTO.getSignature();
        @Nullable final String signatureTarget = createSignature(temp);
        if (signatureSource == null) throw new AccessDeniedException();
        final boolean checkSignature = signatureSource.equals(signatureTarget);
        if (!checkSignature) throw new AccessDeniedException();
        final boolean isContain = sessionRepository.contains(sessionDTO.getId());
        if (!isContain) throw new AccessDeniedException();
    }

    @Override
    @Transactional(readOnly = true)
    public void validate(@Nullable final SessionDTO sessionDTO, @Nullable final Role role) throws Exception {
        if (role == null) return;
        validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findOneById(userId));
        if (userDTO == null) throw new AccessDeniedException();
        if (!role.equals(userDTO.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO open(@Nullable final String login, @Nullable final String password) throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setStartTime(System.currentTimeMillis());
        session.setSignature(createSignature(SessionDTO.toDTO(session)));
        sessionRepository.persist(session);
        return SessionDTO.toDTO(session);
    }

    @Override
    @Transactional
    public void close(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        sessionRepository.removeOneById(sessionDTO.getId());
    }

    @Override
    @Transactional
    public void closeAll(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        sessionRepository.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Transactional
    public void closeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findOneByLogin(login));
        if (userDTO == null) throw new AccessDeniedException();
        @NotNull final String userId = userDTO.getId();
        sessionRepository.removeOneByUserId(userId);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public UserDTO getUser(@Nullable final SessionDTO sessionDTO) throws Exception {
        @Nullable final String userId = getUserId(sessionDTO);
        if (userId == null) return null;
        return UserDTO.toDTO(userService.findOneById(userId));
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public String getUserId(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        return sessionDTO.getUserId();
    }

    @NotNull
    @Override
    @Transactional(readOnly = true)
    public List<SessionDTO> getSessionList(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final List<SessionDTO> sessionsDTO = SessionDTO.toDTO(sessionRepository.findAllByUserId(sessionDTO.getUserId()));
        return sessionsDTO;
    }

    private boolean checkDataAccess(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findOneByLogin(login));
        if (userDTO == null) return false;
        @Nullable final String passwordHash = HashUtil.hidePassword(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(userDTO.getPasswordHash());
    }

    @Nullable
    private String createSignature(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        @Nullable final Integer cycle = propertyService.getSessionCycle();
        return SignatureUtil.sign(sessionDTO, salt, cycle);
    }

}
