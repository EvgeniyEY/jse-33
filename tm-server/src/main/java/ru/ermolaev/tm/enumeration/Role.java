package ru.ermolaev.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
