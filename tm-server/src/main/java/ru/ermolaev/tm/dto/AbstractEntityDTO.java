package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
