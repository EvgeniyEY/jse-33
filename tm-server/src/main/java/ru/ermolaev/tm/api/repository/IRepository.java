package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

    void persist(@NotNull E e);

    void merge(@NotNull E e);

    void remove(@NotNull E e);

}
