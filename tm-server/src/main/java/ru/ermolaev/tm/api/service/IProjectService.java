package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project> {

    void createProject(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateStartDate(@Nullable String userId, @Nullable String id, @Nullable Date date) throws Exception;

    void updateCompleteDate(@Nullable String userId, @Nullable String id, @Nullable Date date) throws Exception;

    @NotNull
    Long countAllProjects();

    @NotNull
    Long countUserProjects(@Nullable String userId) throws Exception;

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable String userId) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void removeAll();

    void removeAllByUserId(@Nullable String userId) throws Exception;

}
