package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findOneById(@Nullable String id) throws Exception;

    @Nullable
    User findOneByLogin(@Nullable String login) throws Exception;

    @NotNull
    List<UserDTO> findAll();

    void removeOneById(@Nullable String id) throws Exception;

    void removeOneByLogin(@Nullable String login) throws Exception;

    void removeAll();

    void create(@Nullable String login, @Nullable String password) throws Exception;

    void create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    void create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @NotNull
    Long count();

    void updatePassword(@Nullable String userId, @Nullable String newPassword) throws Exception;

    void updateUserFirstName(@Nullable String userId, @Nullable String newFirstName) throws Exception;

    void updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName) throws Exception;

    void updateUserLastName(@Nullable String userId, @Nullable String newLastName) throws Exception;

    void updateUserEmail(@Nullable String userId, @Nullable String newEmail) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
