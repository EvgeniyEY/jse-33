package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void validate(@Nullable SessionDTO sessionDTO) throws Exception;

    void validate(@Nullable SessionDTO sessionDTO, @Nullable Role role) throws Exception;

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password) throws Exception;

    void close(@Nullable SessionDTO sessionDTO) throws Exception;

    void closeAll(@Nullable SessionDTO sessionDTO) throws Exception;

    void closeByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDTO getUser(@Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    String getUserId(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    List<SessionDTO> getSessionList(@Nullable SessionDTO sessionDTO) throws Exception;

}
