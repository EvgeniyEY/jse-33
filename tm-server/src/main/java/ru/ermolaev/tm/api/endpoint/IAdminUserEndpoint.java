package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

public interface IAdminUserEndpoint {

    void createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    void createUserWithRole(@Nullable SessionDTO sessionDTO, @Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    void lockUserByLogin(@Nullable SessionDTO sessionDTO, @Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable SessionDTO sessionDTO, @Nullable String login) throws Exception;

    void removeUserByLogin(@Nullable SessionDTO sessionDTO, @Nullable String login) throws Exception;

}
