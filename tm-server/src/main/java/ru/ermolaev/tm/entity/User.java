package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.enumeration.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {

    @NotNull
    @Column(columnDefinition = "TINYTEXT",
            unique = true,
            nullable = false,
            updatable = false)
    private String login = "";

    @NotNull
    @Column(columnDefinition = "TINYTEXT",
            nullable = false)
    private String passwordHash = "";

    @NotNull
    @Column(columnDefinition = "TINYTEXT")
    private String email = "";

    @NotNull
    @Column(columnDefinition = "TINYTEXT")
    private String firstName = "";

    @NotNull
    @Column(columnDefinition = "TINYTEXT")
    private String middleName = "";

    @NotNull
    @Column(columnDefinition = "TINYTEXT")
    private String lastName = "";

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    @Column(columnDefinition = "BIT")
    private Boolean locked = false;

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Session> sessions = new ArrayList<>();

}
