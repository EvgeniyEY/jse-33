package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class Task extends AbstractEntity {

    @Nullable
    @Column(nullable = false, columnDefinition = "TINYTEXT")
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date completeDate;

    @Nullable
    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

}
