package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

@Repository
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Long countByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.project.id = :projectId", Long.class)
                .setParameter("projectId", projectId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        final List<Task> tasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList();
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Task> tasks = entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultList();
        return tasks.get(0);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.project.id = :projectId", Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Task> tasks = findAll();
        for (@NotNull final Task task : tasks) {
            entityManager.remove(task);
        }
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAllByUserId(userId);
        for (@NotNull final Task task : tasks) {
            entityManager.remove(task);
        }
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> tasks = findAllByProjectId(projectId);
        for (@NotNull final Task task : tasks) {
            entityManager.remove(task);
        }
    }

    @Override
    public void removeAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<Task> tasks = findAllByUserIdAndProjectId(userId, projectId);
        for (@NotNull final Task task : tasks) {
            entityManager.remove(task);
        }
    }

}
