package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

@Repository
public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        final List<Project> projects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList();
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        final List<Project> projects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultList();
        return projects.get(0);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Project> projects = findAll();
        for (@NotNull final Project project : projects) {
            entityManager.remove(project);
        }
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAllByUserId(userId);
        for (@NotNull final Project project : projects) {
            entityManager.remove(project);
        }
    }

}
