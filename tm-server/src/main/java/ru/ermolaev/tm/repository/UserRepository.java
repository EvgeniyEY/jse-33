package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.entity.User;

import java.util.List;

@Repository
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM User e", Long.class).getSingleResult();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        final List<User> users = entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .getResultList();
        return users.get(0);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final User user = findOneById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeAll() {
        @NotNull final List<User> users = findAll();
        for (@NotNull final User user : users) {
            entityManager.remove(user);
        }
    }

}
