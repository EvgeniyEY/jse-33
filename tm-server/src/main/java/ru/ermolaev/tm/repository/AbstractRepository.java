package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepository<E extends AbstractEntity> {

    @PersistenceContext
    protected EntityManager entityManager;

    public void persist(@NotNull final E e) {
        entityManager.persist(e);
    }

    public void merge(@NotNull final E e) {
        entityManager.merge(e);
    }

    public void remove(@NotNull final E e) {
        entityManager.remove(e);
    }

}
