package ru.ermolaev.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownIdException extends AbstractException {

    public UnknownIdException() {
        super("Error! This ID does not exist.");
    }

    public UnknownIdException(@NotNull final String id) {
        super("Error! This ID [" + id + "] does not exist.");
    }

}
