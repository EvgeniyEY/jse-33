package ru.ermolaev.tm.exception.user;

import ru.ermolaev.tm.exception.AbstractException;

public final class UserAlreadyExistException extends AbstractException {

    public UserAlreadyExistException() {
        super("Error! User with this login already exist in system. Try with another login.");
    }

}
