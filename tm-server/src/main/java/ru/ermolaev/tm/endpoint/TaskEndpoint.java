package ru.ermolaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ermolaev.tm.api.endpoint.ITaskEndpoint;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public final class TaskEndpoint implements ITaskEndpoint {

    private ISessionService sessionService;

    private ITaskService taskService;

    @Autowired
    public TaskEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ITaskService taskService
    ) {
        this.sessionService = sessionService;
        this.taskService = taskService;
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.createTask(sessionDTO.getUserId(), taskName, projectName, description);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.updateById(sessionDTO.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.updateStartDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateTaskCompleteDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.updateCompleteDate(sessionDTO.getUserId(), id, date);
    }

    @NotNull
    @Override
    @WebMethod
    public Long countAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return taskService.countAllTasks();
    }

    @NotNull
    @Override
    @WebMethod
    public Long countUserTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return taskService.countByUserId(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Long countProjectTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return taskService.countByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Long countUserTasksOnProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return taskService.countByUserIdAndProjectId(sessionDTO.getUserId(), projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return TaskDTO.toDTO(taskService.findOneById(sessionDTO.getUserId(), id));
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return TaskDTO.toDTO(taskService.findOneByName(sessionDTO.getUserId(), name));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return taskService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return taskService.findAllByUserId(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return taskService.findAllByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllUserTasksOnProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return taskService.findAllByUserIdAndProjectId(sessionDTO.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.removeOneById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.removeOneByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        taskService.removeAll();
    }

    @Override
    @WebMethod
    public void clearTasksByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void clearTasksByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.removeAllByProjectId(projectId);
    }

    @Override
    @WebMethod
    public void clearUserTasksOnProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        sessionService.validate(sessionDTO);
        taskService.removeAllByUserIdAndProjectId(sessionDTO.getUserId(), projectId);
    }

}
